<?php

namespace App\Http\Controllers\Admin;

use App\Photo;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhotoController extends Controller
{
    public function store(Post $post)
    {
        $this->validate(request() ,
       [
            'photo' => 'required|image|max:2048' // sera png jpg gif svg bmp
       ]);
      /* $photo =  request()->file('photo');
       // elegimos el store public
       $photoUrl = $photo->store('public');
       // te guardara la imagen en ese formato
       // /storage/vHDj7VzXVAF6vBqNnNd20ccbdlik49rCsaJFIecp.png
       // Storage::url($photoUrl);*/

     Photo::create([
           // se guardara dentro storage/app/public/posts
            'url' =>request()->file('photo')->store('posts','public'),
           'post_id' => $post->id
       ]);
    }

    public function destroy(Photo $photo)
    {
        // lo eliminamos de bd
        $photo->delete();
        // dentro del modelo Photo le pasamos un boot
        // para qye cuando se este elimnando tambien se elimine deL proyecto
        return back()->with('message', ['success', __('Foto eliminada correctamente')]);

    }
}
