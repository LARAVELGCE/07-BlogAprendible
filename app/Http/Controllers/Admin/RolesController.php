<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Http\Requests\SaveRolesRequest;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

            return view('admin.roles.index', [
                'roles' => Role::all()
                ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',$role = new Role);
        $btnText = __("Crear Role");
        return view('admin.roles.form', [
            'role' => $role,
            'permissions' => Permission::pluck('name', 'id'),

        ],compact('btnText'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveRolesRequest  $request)
    {
        $this->authorize('create', new Role);

        $role = Role::create($request->validated());


       if ($request->has('permissions'))
        {
            // le asigno los permisos al rol
            $role->givePermissionTo($request->permissions);
        }

        return redirect()->route('roles.index')->with('message', ['success', __('El role fue creado correctamente')]);

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $this->authorize('update', $role);

        $btnText = __("Editar Role");
        //
        return view('admin.roles.form', [
            'role' => $role,
            'permissions' => Permission::pluck('name', 'id')
        ],compact('btnText'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveRolesRequest  $request, Role $role)
    {
        $this->authorize('update', $role);

        $role->update($request->validated());

        // elimina todas sus relaciones el rol con sus permisos
        // y los elimina
        $role->permissions()->detach();

        // si es que tiene permisos el rol los va a asociar nuevamente
        if ($request->has('permissions'))
        {
            // tabla role_has_permissions
            $role->givePermissionTo($request->permissions);
        }

        return redirect()->route('roles.edit', $role)->with('message', ['success', __('El role fue actualizado correctamente')]);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $this->authorize('delete',$role);

        try {
            $role->delete();
            return back()->with('message', ['success', __("Role eliminado correctamente")]);
        } catch (\Exception $exception) {
            return back()->with('message', ['danger', __("Error eliminando el Role")]);
        }
    }
}
