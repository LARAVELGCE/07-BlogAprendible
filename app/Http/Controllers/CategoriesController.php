<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function searchCategory(Category $category)
    {
        // Estamos enviado a pantala del welcome solo las post de dicha categoria
          return view('pages.home',
              [
                 // Estamos enviado la categoria
                  'title' => "Publicacion de la categoria {$category->name}" ,
                  // Estamos enviado a pantala del welcome solo las post de dicha categoria
                  'posts' => $category->posts()->published()->paginate(2)

              ]);
    }
}
