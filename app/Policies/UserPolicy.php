<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    // para que los administradaores miren editen eliminien crea (cualquier accion que estan abajos )
    // solo para el usuario administrador
    // retorna
    // AQUI EL ADMIN TIENE ACCESO A TODAS LAS FUNCIONES CRUD
    public function before($user)
    {
        if ( $user->hasRole('Admin') )
        {
            return true;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $authUser, User $user)
    {
        // $authUser =   usuario autenticado
        // $user     =   el id del usuario

        // Si es que no eres un administrador solo podras ver a ti mismo
        return $authUser->id === $user->id || $user->hasPermissionTo('View users');
    }


    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('Create users');
    }


    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $authUser, User $user)
    {
        // $authUser =   usuario autenticado
        // $user     =   el id del usuario
        return $authUser->id === $user->id || $user->hasPermissionTo('Update users');
    }


    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $authUser, User $user)
    {
        return $authUser->id === $user->id || $user->hasPermissionTo('Delete users');
    }

}
