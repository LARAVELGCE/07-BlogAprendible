<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// usamos este facades para poder manejar el storage
use Illuminate\Support\Facades\Storage;

class Photo extends Model
{
    // desabilitamos la asignacion masiva
    protected  $guarded = [];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function pathAttachment () {
        return "/storage/" . $this->url;
    }

    // Cuando se esta eliminando la foto lo eliminamos
    // del disco
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($photo) {
            if (!\App::runningInConsole()) {
                Storage::disk('public')->delete($photo->url);
            }
        });
    }

}
