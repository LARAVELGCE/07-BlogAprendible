<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Post extends Model
{

    // para que laravel se de cuenta que tambien tiene formato de fecha
    protected $dates = ['published_at'];
    // solo los campos que estas asignado se van a actualizar
    protected $fillable = ['title', 'excerpt', 'body','iframe', 'category_id','published_at','slug','user_id'];



    public static function boot ()
    {
        parent::boot();


        // Cuando se este eliminado se elimine sus relaciones
        static::deleting(function($post){
            if (!\App::runningInConsole()) {
                $post->tags()->detach();
                $post->photos->each->delete();
            }
        });

    }



    // Cada post tiene una categoria
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function photos() {
        return $this->hasMany(Photo::class);
    }

    // Cada post tiene un usuario
    public function owner()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function scopePublished($query)
    {
        $query->with(['category','tags','owner','photos'])
            ->whereNotNull('published_at')
            ->where('published_at','<=', Carbon::now())
            ->latest('published_at');
    }

    // scope que sirve para listar los post solo del admin(todos)
    // y el escritor que filtre solo sus posts
    public function scopeallowed($query)
    {
        // Le pasamos la funcion can  los datos del $post
        // esto se ira al metodo Policies/PostPolicy y vera la funcion view
        // la cual dice que si el post le pertenece o sino si es que
        // tiene el permiso View post (TABLA PERMISSIONS Y MODEL_HAS_PERMISSIONS) si es que estan asociados pueden ver todos los post
        if (auth()->user()->can('view',$this))
        {
            return  $query;
        }
        else
        {
            // aqui el user_id de la tabla post debe de ser igual al id
            // del usuario autenticado
            return $query->where('user_id', auth()->id());
        }
    }

    // Slug para redifirigir a cda post por detalle
    public function getRouteKeyName() {
        return 'slug';
    }

   // $postRequest->get('published_at') esto lo recibimos de PostController
    //y es recibida como parametro ($published_at );
    public function setPublishedAtAttribute($published_at)
    {
        $now = Carbon::now();
        //                                   si tiene un valor
        $this->attributes['published_at'] = $published_at
        // procesmos la fecha con carbon
                                        ? Carbon::parse($published_at . $now->format('H:i:s'))->toDateTimeString()
            // de lo contrario devolvemos nulo
                                        : null;
    }

    //   $postRequest->get('category_id') esto lo recibimos de PostController
    //y es recibida como parametro ($category_id );
    public function setCategoryIdAttribute($category_id)
    {
        $this->attributes['category_id'] = Category::find($category_id)
                                        ? $category_id
                                        : Category::create(['name' => $category_id ])->id;
    }

    public function syncTags($tags)
    {
        $tagId = collect($tags)->map(function ($tag)
            {
                 // si lo encuentro solo retorno las etiquetas que estan ahi
                return   Tag::find($tag)
                        ? $tag
                        // de lo contrario los creo y obtenemos sus id
                        : Tag::create(['name' => $tag ])->id;
            });
        // como estamos dentro del modelo Post ya no es necesario $post->
        //  $post->tags()->sync($tags);
        return $this->tags()->sync($tagId);
    }

    //   $post = Post::create([ 'title' => $postRequest->get('title')]);
    // Dentro de mi Controlador Admin/PostsController dentro del metodo store
    // sobreescribire el me metodo pero previamnte le pasamos los atributos
    public static function create( $attributes = [])
    {
        $post = static::query()->create($attributes);

        $post->generateUrl();

        return $post;
    }

    public function generateUrl()
    {
        $url = str_slug($this->title);

        // si existe el slug a ese slug solo le agregas su id
        if ($this->whereSlug($url)->exists())
        {
            $url = "{$url}-{$this->id}";
        }

        $this->slug = $url;

        $this->save();
    }

    // Para que sea publica mi post
    // la fecha del post debe de ser diferente de nulo
    // ademas debe de ser menor a la fecha actual
    public function isPublished()
    {
        return ! is_null($this->published_at) && $this->published_at < today();
    }

    public function scopeByYearAndMonth($query)
    {
        return $query->selectRaw('year(published_at) year')
            ->selectRaw('month(published_at) month')
            ->selectRaw('monthname(published_at) monthname')
            ->selectRaw('count(*) posts')
            ->groupBy('year', 'month', 'monthname')
            ->orderBy('published_at');
    }


    public function viewType($home = '')
    {
        if ($this->photos->count() === 1) :
        return 'posts.photo';
        elseif($this->photos->count() > 1):
            return $home === 'home' ?  'posts.carousel-preview' : 'posts.carousel';
        elseif($this->iframe):
            return 'posts.iframe';
        else:
            return 'posts.text';
        endif;
    }





}
