@extends('admin.layout')
@section('header')
    <h1>
       Posts
        <small>Crear publicacion</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li><a href="{{ route('posts.index') }}"><i class="fa fa-dashboard"></i>Posts</a></li>
    </ol>
@endsection
@section('content')
    <div class="row">
        {{--si es que tiene imagen muestra las fotos de lo contrario no muestra nada--}}
        @if ($post->photos->count())
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            @foreach ($post->photos as $photo)
                                <form method="POST" action="{{ route('admin.photos.destroy',$photo) }}">
                                    @csrf
                                    @method('DELETE')
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-danger btn-xs" style="position: absolute">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                        <img class="img-responsive" src="/storage/{{ $photo->url }}" alt="">
                                    </div>
                                </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <form
                method="POST"
                {{--Si no exite el id del curso se va procesar store (guardar una nuevo )
                si lo tiene solo va actualizar el id del regustro y ademas le paso el slug --}}
                action="{{ ! $post->id ? route('posts.store') : route('posts.update', ['slug' => $post->slug])}}"
                {{--que no valide el navegador --}}
                novalidate
                {{--subida de archivos --}}
                enctype="multipart/form-data"

        >

            {{--si estamos en modo edicion usamos el metodo PUT--}}
            @if($post->id)
                @method('PUT')
            @endif

            @csrf
            <div class="col-md-8">

                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">Crear Post</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }} has-feedback">
                                <label for="name">
                                    {{ __("Titulo de la publicacion") }}
                                </label>
                                    <input
                                            name="title"
                                            id="title"
                                            class="form-control"
                                            value="{{ old('title') ?: $post->title }}"
                                            placeholder="Ingrese aqui el titulo de la publicacion"
                                            autofocus
                                    />

                                    @if ($errors->has('title'))
                                        <span class="help-block" role="alert">
                                                {{ $errors->first('title') }}
                                            </span>
                                    @endif
                             </div>
                            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }} has-feedback">
                                <label for="body">
                                    {{ __("Contenido de la publicacion") }}
                                </label>
                                <textarea
                                        name="body"
                                        id="editor"
                                        class="form-control"
                                        placeholder="Ingrese aqui el contenido completo de la publicacion"
                                        autofocus
                                        rows="10"
                                >{{ old('body') ?: $post->body }}</textarea>

                                @if ($errors->has('body'))
                                    <span class="help-block" role="alert">
                                                {{ $errors->first('body') }}
                                            </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback">
                                <label for="iframe">
                                    {{ __("Contendido Embebido (Iframe)") }}
                                </label>
                                <textarea
                                        name="iframe"
                                        id="iframe"
                                        class="form-control"
                                        autofocus
                                        placeholder="Ingresa contendido embebido (iframe) de audio o video"
                                        rows="2"
                                >{{ old('iframe') ?: $post->iframe }}</textarea>

                            </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-body">

                        <div class="form-group {{ $errors->has('published_at') ? ' has-error' : '' }} has-feedback">
                            <label for="published_at">
                                {{ __("Fecha de Publicacion") }}
                            </label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input
                                        name="published_at"
                                        id="datepicker"
                                        class="form-control pull-right"
                                        value="{{ old('published_at') ?: ( $post->published_at ? $post->published_at->format('m/d/Y') : null ) }}"
                                />
                                @if ($errors->has('published_at'))
                                    <span class="help-block" role="alert">
                                                        {{ $errors->first('published_at') }}
                                                    </span>
                                @endif
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group {{ $errors->has('tags') ? ' has-error' : '' }}">
                            <label for="tags" >{{ __("Tag del Posts") }}</label>
                            <select name="tags[]" id="tags" class="form-control select2" multiple="multiple" data-placeholder="Selecciona una o mas etiquetas" style="width: 100%">
                                @foreach(\App\Tag::groupBy('name')->pluck('name', 'id') as $id => $tag)
                                    {{--Preguntamos si el old('tags') contiene el id si es verdadero se agrega la clase select--}}
                                    <option {{ (int) collect(old('tags',$post->tags->pluck('id')))->contains($id) ? 'selected' : '' }} value="{{ $id }}">
                                        {{ $tag }}
                                    </option>
                                @endforeach
                            </select>
                            @if ($errors->has('tags'))
                                <span class="help-block" role="alert">
                                                        {{ $errors->first('tags') }}
                                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }} has-feedback">
                            <label for="category_id" >{{ __("Categoría del Posts") }}</label>
                                <select name="category_id" id="category_id" class="form-control select2">
                                    {{--le agrego un order by porque para evitar que se repitan las categorias
                                    si se cae por el groupBy entonces anda config/database y cambia  'strict' => false --}}
                                    <option  value="">
                                        {{ __("Seleccione una categoria") }}
                                    </option>
                                    @foreach(\App\Category::groupBy('name')->pluck('name', 'id') as $id => $category)
                                        <option {{ (int) old('category_id') === $id || $post->category_id === $id ? 'selected' : '' }} value="{{ $id }}">
                                            {{ $category }}
                                        </option>
                                    @endforeach
                                </select>
                            @if ($errors->has('category_id'))
                                <span class="help-block" role="alert">
                                                        {{ $errors->first('category_id') }}
                                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('excerpt') ? ' has-error' : '' }} has-feedback">
                            <label for="excerpt">
                                {{ __("Extracto  de la publicacion") }}
                            </label>
                            <textarea
                                    name="excerpt"
                                    id="excerpt"
                                    class="form-control"
                                    placeholder="Ingrese aqui el extracto  de la publicacion"
                                    autofocus
                                    rows="3"
                            >{{ old('excerpt') ?: $post->excerpt }}</textarea>
                            @if ($errors->has('excerpt'))
                                <span class="help-block" role="alert">
                                                        {{ $errors->first('excerpt') }}
                                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="dropzone">

                            </div>
                        </div>
                        <div class="form-group">
                           <button type="submit" class="btn btn-primary btn-block">
                               {{ __($btnText) }}
                           </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css" rel="stylesheet">
    <link href="/css/datepicker3select2.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    <script src="/js/datepicker3select2.js" ></script>
    <script>
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        // iniciar el select 2
        $(".select2").select2(
            {
                // permite escribit y agregar como un dato al select de
                // categoria como los tags por le ponemos tags : true
                tags : true
            }
        );
        // iniciar el content
        CKEDITOR.replace('editor');
        // alto del contendido   del formulario
        CKEDITOR.config.height = 315;

        // que inicie dropzone con la variable dropzone que se declaro en el html
        // lo guardamos en una variables
       var mydropzone =  new Dropzone('.dropzone',{
            url : '/admin/posts/{{ $post->slug }}/photos',
            paramName : 'photo',
            // validamos que los archivos subidos sean imagenes con cualquierea extension por eso le ponemos *
            acceptedFiles : 'image/*',
            // tamaño de la imagen de 2MB
            maxFilesize: 2 ,
           // cantidad de Imagenes
           //maxFiles: 1 ,
            // le pasamos el token
            headers : {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            // cambiamos el mensaje por defecto que aparece en el panel de imagenes
            dictDefaultMessage : 'Arrastra las fotos para subirlas'
        });

       // aca escuchamos los erros y enviamos con una funcion
        // el archivo , y la respuesta del servidor
       mydropzone.on('error', function (file, res) {
           // console.log(res.errors.photo[0]);
           var msg = res.errors.photo[0];
            $('.dz-error-message:last > span').text(msg);
       });
        // que no lo inicie automaticamente
        Dropzone.autoDiscover = false;

    </script>
@endpush