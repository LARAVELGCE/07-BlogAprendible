<ul class="sidebar-menu">
    <li class="header">Navegacion</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="{{ setActiveRoute('home') }}">
        <a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> <span>Inicio</span>
        </a>
    </li>

    <li class="treeview {{ setActiveRoute('posts.index') }}">
        <a href="#"><i class="fa fa-bars"></i> <span>Blog</span>
            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
             </span>
        </a>
        <ul class="treeview-menu">
            <li class="{{ setActiveRoute('posts.index')}}">
                <a href="{{ route('posts.index') }}"><i class="fa fa-eye"></i>{{ __("Ver todos los post") }}</a>
            </li>

            @can('create', new App\Post)
                <li>
                    {{--Si esta en esa ruta se abre inmediatamente el modal --}}
                    @if (request()->is('admin/posts/*'))
                        <a href="{{ route('posts.index', '#create') }}"><i class="fa fa-pencil"></i> {{ __("Crear un post") }}</a>
                    @else
                        <a href="#"  data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i>{{ __("Crear un post") }}</a>
                    @endif
                </li>
             @endcan
        </ul>
    </li>

    {{--Nav de User--}}
    @can('view', new App\User)
    <li class="treeview {{ setActiveRoute(['users.index','users.create']) }}">
        <a href="#"><i class="fa fa-users"></i> <span>Usuarios</span>
            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
             </span>
        </a>
        <ul class="treeview-menu">
            <li class="{{ setActiveRoute('users.index')}}">
                <a href="{{ route('users.index') }}"><i class="fa fa-eye"></i>{{ __("Ver todos los Usuarios") }}</a>
            </li>
            <li>
                <a href="{{ route('users.create') }}"><i class="fa fa-pencil"></i> {{ __("Crear un Usuario") }}</a>
            </li>
        </ul>
    </li>
     @else
        <li class="{{ setActiveRoute(['users.show']) }}">
            <a href="{{ route('users.show', auth()->user()) }}">
                <i class="fa fa-user"></i> <span>Perfil</span>
            </a>
        </li>

    @endcan


    @can('view', new \Spatie\Permission\Models\Role)
        <li class="{{ setActiveRoute(['roles.index']) }}">
            <a href="{{ route('roles.index') }}">
                <i class="fa fa-pencil"></i> <span>Roles</span>
            </a>
        </li>
    @endcan

    @can('view', new \Spatie\Permission\Models\Permission)
        <li class="{{ setActiveRoute(['permissions.index']) }}">
            <a href="{{ route('permissions.index') }}">
                <i class="fa fa-pencil"></i> <span>Permissions</span>
            </a>
        </li>
    @endcan



</ul>