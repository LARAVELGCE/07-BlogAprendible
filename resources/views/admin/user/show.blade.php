@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle"
                         src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg"
                         alt="{{ $user->name }}">

                    <h3 class="profile-username text-center">{{ $user->name }}</h3>

                    <p class="text-muted text-center">{{ $user->getRoleNames()->implode(', ') }}</p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Email</b> <a class="pull-right">{{ $user->email }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Publicaciones</b> <a class="pull-right">{{ $user->posts->count() }}</a>
                        </li>
                        @if ($user->roles->count())
                            <li class="list-group-item">
                                <b>Roles</b> <a class="pull-right">{{ $user->getRoleNames()->implode(', ') }}</a>
                            </li>
                        @endif
                    </ul>

                    <a href="{{ route('users.edit', $user) }}" class="btn btn-primary btn-block"><b>Editar</b></a>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Publicaciones</h3>
                </div>
                <div class="box-body">
                    @forelse ($user->posts as $post)
                        <a href="{{ route('posts.detail', $post) }}" target="_blank">
                            <strong>{{ $post->title }}</strong>
                        </a>
                        <br>
                        <small class="text-muted">Publicado el {{ $post->published_at->format('d/m/Y') }}</small>
                        <p class="text-muted">{{ $post->excerpt }}</p>
                        {{--En caso de que no sea la ultima publicacion --}}
                        {{--Desaparece el hr --}}
                        @unless ($loop->last)
                            <hr>
                        @endunless
                    @empty
                        <small class="text-muted">No tiene ninguna publicación</small>
                    @endforelse
                </div>
            </div>

        </div>
        <div class="col-md-3">
        <!--
                    tabla : model_has_roles
           role_id      model_type         model_id
            1	        App\User	        1
            2	        App\User	        1

            Significa que el usuario 1 osea que viene a hacer model_id = user_id
            tiene dos roles tanto admin y writer
        -->


            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Roles</h3>
                </div>
                <div class="box-body">
                    @forelse ($user->roles as $role)
                        <strong>{{ $role->name }}</strong>
                        @if ( $role->permissions->count() )
                            <br>
                            <small class="text-muted">
                                Permisos: {{ $role->permissions->pluck('name')->implode(', ') }}
                            </small>
                        @endif
                        @unless ($loop->last)
                            <hr>
                        @endunless
                    @empty
                        <small class="text-muted">No tiene ningún role asignado</small>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <!--
                    tabla : model_has_permissions
           permission_id      model_type         model_id
            1	        App\User	        1
            2	        App\User	        1
            3	        App\User	        1

            Significa que el usuario 1 osea que viene a hacer model_id = user_id
            tiene 3 permisos tanto el 1 2 y el 3
            View posts | Create posts | Update posts
        -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Permisos adicionales</h3>
                </div>
                <div class="box-body">
                    @forelse ($user->permissions as $permission)
                        <strong>{{ $permission->name }}</strong>
                        @unless ($loop->last)
                            <hr>
                        @endunless
                    @empty
                        <small class="text-muted">No tiene permisos adicionales</small>
                    @endforelse
                </div>
            </div>
        </div>
    </div>

@stop