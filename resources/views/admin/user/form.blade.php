@extends('admin.layout')
@section('header')

@endsection
@section('content')
    <div class="row">

        <form
                method="POST"
                {{--Si no exite el id del curso se va procesar store (guardar una nuevo )
                si lo tiene solo va actualizar el id del regustro y ademas le paso el slug --}}
                action="{{route('users.update', ['id' => $user->id])}}"
                {{--que no valide el navegador --}}
                novalidate
                {{--subida de archivos --}}
                enctype="multipart/form-data"
        >

            @csrf  @method('PUT')


            <div class="col-md-6">

                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">Datos Personales</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                            <label for="name">
                                {{ __("Nombre") }}
                            </label>
                            <input
                                    name="name"
                                    id="name"
                                    class="form-control"
                                    value="{{ old('name') ?: $user->name }}"

                            />

                            @if ($errors->has('name'))
                                <span class="help-block" role="alert">
                                                {{ $errors->first('name') }}
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                            <label for="email">
                                {{ __("Email") }}
                            </label>
                            <input
                                    type="email"
                                    name="email"
                                    id="email"
                                    class="form-control"
                                    value="{{ old('email') ?: $user->email }}"

                            />

                            @if ($errors->has('email'))
                                <span class="help-block" role="alert">
                                                {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                            <label for="password">
                                {{ __("Contraseña") }}
                            </label>
                            <input
                                    type="password"
                                    name="password"
                                    id="password"
                                    class="form-control"

                            />

                            @if ($errors->has('password'))
                                <span class="help-block" role="alert">
                                                {{ $errors->first('password') }}
                                </span>
                            @endif
                            <span class="help-block">
                                                  {{ __("Dejar en blanco si no quieres cambiar la contraseña") }}
                                </span>
                        </div>

                        <div class="form-group">
                            <label for="password">
                                {{ __("Repite Contraseña") }}
                            </label>
                            <input
                                    type="password"
                                    name="password_confirmation"
                                    id="password_confirmation"
                                    class="form-control"

                            />

                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __($btnText) }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
            <div class="col-md-6">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Roles</h3>
                    </div>
                    <div class="box-body">
                        @role('Admin')
                        {{--Y pasarle al usuario a quien actualizar su rol --}}
                        <form method="POST" action="{{ route('users.roles.update', $user) }}">
                           @csrf @method('PUT')
                            @include('admin.roles.checkboxes')
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __("Actualizar Roles") }}
                                </button>
                            </div>
                        </form>
                        @else
                            <ul class="list-group">
                                @forelse ($user->roles as $role)
                                    <li class="list-group-item">{{ $role->name }}</li>
                                @empty
                                    <li class="list-group-item">No tiene roles</li>
                                @endforelse
                            </ul>
                          @endrole

                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Permisos</h3>
                    </div>
                    <div class="box-body">
                        @role('Admin')
                        {{--Y pasarle al usuario a quien actualizar su rol --}}
                        <form method="POST" action="{{ route('users.permissions.update', $user) }}">
                            @csrf @method('PUT')
                            @include('admin.permissions.checkboxes', ['model' => $user])
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __("Actualizar Permisos") }}
                                </button>
                            </div>
                        </form>
                        @else
                            <ul class="list-group">
                                @forelse ($user->permissions as $permission)
                                    <li class="list-group-item">{{ $permission->name }}</li>
                                @empty
                                    <li class="list-group-item">No tiene permisos</li>
                                @endforelse
                            </ul>
                         @endrole

                    </div>
                </div>

            </div>

    </div>

@endsection
