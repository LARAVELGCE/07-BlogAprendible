<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Iniciar y eliminar para volver a ejecutar los curso
        // usamos el disco public por que en el config/filesystem apunta
        // a  app/public  y luego apuntara al posts
        //
       /* 'public' => [

        'root' => storage_path('app/public'),

        ],*/


        Storage::disk('public')->deleteDirectory('posts');

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(UserRoleTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        factory(\App\User::class,10)->create();
        factory(\App\Category::class, 10)->create();
        factory(\App\Tag::class,20)->create();
        factory(\App\Post::class,50)->create();


    }
}
