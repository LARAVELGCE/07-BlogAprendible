let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/plantilla/css/normalize.css',
    'resources/assets/plantilla/css/framework.css',
    'resources/assets/plantilla/css/style.css',
    'resources/assets/plantilla/css/responsive.css'
], 'public/css/plantilla.css');


mix.styles([
    'resources/assets/adminLTE/bootstrap/css/bootstrap.min.css',
    'resources/assets/adminLTE/css/ionicons.min.css',
    'resources/assets/adminLTE/css/AdminLTE.min.css',
    'resources/assets/adminLTE/css/skins/skin-blue.min.css',
], 'public/css/adminLTE.css');


mix.scripts([
    'resources/assets/adminLTE/plugins/jQuery/jquery-2.2.3.min.js',
    'resources/assets/adminLTE/bootstrap/js/bootstrap.min.js',
    'resources/assets/adminLTE/js/app.min.js',
], 'public/js/adminLTE.js');


// datapicker y select 2 css
mix.styles([
    'resources/assets/adminLTE/plugins/datepicker/datepicker3.css',
    'resources/assets/adminLTE/plugins/select2/select2.min.css'
], 'public/css/datepicker3select2.css');

// datapicker y select2  js
mix.scripts([
    'resources/assets/adminLTE/plugins/select2/select2.full.min.js',
    'resources/assets/adminLTE/plugins/datepicker/bootstrap-datepicker.js'
], 'public/js/datepicker3select2.js');

// datatables css
mix.styles([
    'resources/assets/adminLTE/plugins/datatables/dataTables.bootstrap.css',
], 'public/css/datatables.css');

// datatables js
mix.scripts([
    'resources/assets/adminLTE/plugins/dataTables/jquery.dataTables.min.js',
    'resources/assets/adminLTE/plugins/dataTables/dataTables.bootstrap.min.js',
], 'public/js/datatables.js');

mix.scripts([
    'resources/assets/plantilla/js/bootstrap_carousel.js'
], 'public/js/bootstrap_carousel.js');

mix.styles([
    'resources/assets/plantilla/css/bootstrap_carousel.css'
], 'public/css/bootstrap_carousel.css');