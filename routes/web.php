<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'PagesController@index')->name('pages.home');
Route::get('nosotros', 'PagesController@about')->name('pages.about');
Route::get('archivo', 'PagesController@archive')->name('pages.archive');
Route::get('contacto', 'PagesController@contact')->name('pages.contact');


// public function show(Post $post) recuerda que necesita un parametro $post = post
Route::get('blog/{post}', 'PostsController@show')->name('posts.detail');
// Buscar por categoria
Route::get('/categorias/{category}', 'CategoriesController@searchCategory')->name('search.category');
// Buscar por etiquetas - tags
Route::get('/tags/{tag}', 'TagsController@searchTag')->name('search.tag');
// Rutas para obtener y procesar la suscripcion
Route::group(['middleware' => ['auth']], function () {
    Route::group(["prefix" => "admin", 'namespace' => 'admin'], function() {


        Route::get('/', 'HomeController@index')->name('home');
        // Administrar los post y user
        Route::resource('posts', 'PostsController',['except' => 'show']);
        Route::resource('users', 'UsersController');
        // Administrar Roles
        Route::resource('roles', 'RolesController',['except' => 'show']);
        // Administrar Permisos
        Route::resource('permissions', 'PermissionsController', ['only' => ['index', 'edit', 'update']]);


        // Administrar los roles y solo pueden actualizar los roles los que tienen rol de admnistrador
        Route::middleware('role:Admin')->put('users/{user}/roles', 'UsersRolesController@update')->name('users.roles.update');
        // Administrar los permisos  y solo pueden actualizar los permisos los que tienen rol de admnistrador
        Route::middleware('role:Admin')->put('users/{user}/permissions', 'UsersPermissionsController@update')->name('users.permissions.update');

        // Administrar las photos
        Route::post('posts/{post}/photos', 'PhotoController@store')->name('posts.photos.store');
        Route::delete('photos/{photo}', 'PhotoController@destroy')->name('admin.photos.destroy');
    });

});

Route::auth();